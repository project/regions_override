# Regions Override

Remove regions from rendering on a page. This can be done by content type or on
a page by page basis.

Go into the current theme's appearance page `admin/appearance` and then select
which Regions will belong in the Header, Sidebar, and Footer sections.

Each page can now choose which of these sections to show or hide.

Theme suggestions are provided for the page.html.twig template.
