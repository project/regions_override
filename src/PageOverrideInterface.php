<?php

namespace Drupal\regions_override;

/**
 * Defines the interface for the page override system.
 */
interface PageOverrideInterface {

  /**
   * Returns the stored value for a given key.
   *
   * @param string|null $key
   *   The key of the data to retrieve.
   * @param mixed $default
   *   The default value to use if the key is not found.
   *
   * @return mixed
   *   The stored value, or NULL if no value exists.
   */
  public function get(?string $key, mixed $default = NULL): mixed;

  /**
   * Saves a value for a given key.
   *
   * @param string $key
   *   The key of the data to store.
   * @param mixed $value
   *   The data to store.
   */
  public function set(string $key, mixed $value);

  /**
   * Deletes an item.
   *
   * @param string $key
   *   The item name to delete.
   */
  public function delete(string $key);

  /**
   * Resets the static cache.
   *
   * This is mainly used in testing environments.
   */
  public function resetCache();

  /**
   * Get an override for the current page/route.
   *
   * @return int|null
   *   The type of override used for the page or NULL if no override is set.
   */
  public function getOverride(): ?int;

  /**
   * Create a key based on the current page entity or route.
   *
   * @return string|null
   *   A key based on the page entity info or fallback to page route.
   */
  public function generatePageKey(): ?string;

}
