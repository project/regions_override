<?php

namespace Drupal\regions_override;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\Core\KeyValueStore\KeyValueStoreInterface;
use Drupal\Core\Path\PathMatcherInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\node\NodeInterface;

/**
 * Provides the individual page override system using a key value store.
 */
class PageOverride implements PageOverrideInterface {

  /**
   * The key value store to use.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  protected KeyValueStoreInterface $keyValueStore;

  /**
   * Static override cache.
   *
   * @var array
   */
  protected array $cache = [];

  /**
   * Constructs a PageOverride object.
   *
   * @param \Drupal\Core\KeyValueStore\KeyValueFactoryInterface $keyValueFactory
   *   The key value factory.
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   The currently active route match object.
   * @param \Drupal\Core\Path\PathMatcherInterface $pathMatcher
   *   The path matcher service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory service.
   */
  public function __construct(
    protected KeyValueFactoryInterface $keyValueFactory,
    protected RouteMatchInterface $routeMatch,
    protected PathMatcherInterface $pathMatcher,
    protected ConfigFactoryInterface $configFactory,
  ) {
    $this->keyValueStore = $keyValueFactory->get('regions_override');
  }

  /**
   * {@inheritdoc}
   */
  public function get(?string $key, mixed $default = NULL): mixed {
    if ($key === NULL) {
      return $default;
    }

    $value = NULL;

    // Check if we have a value in the cache.
    if (isset($this->cache[$key])) {
      $value = $this->cache[$key];
    }
    // Load the value if we don't have an explicit NULL value.
    elseif (!array_key_exists($key, $this->cache)) {
      $loaded_value = $this->keyValueStore->get($key);

      if (isset($loaded_value)) {
        $value = $loaded_value;
        $this->cache[$key] = $loaded_value;
      }
      else {
        $this->cache[$key] = NULL;
      }
    }

    return $value ?? $default;
  }

  /**
   * {@inheritdoc}
   */
  public function set(string $key, mixed $value) {
    $this->cache[$key] = $value;
    $this->keyValueStore->set($key, $value);
  }

  /**
   * {@inheritdoc}
   */
  public function delete(string $key) {
    unset($this->cache[$key]);
    $this->keyValueStore->delete($key);
  }

  /**
   * {@inheritdoc}
   */
  public function resetCache() {
    $this->cache = [];
  }

  /**
   * {@inheritdoc}
   */
  public function getOverride(): ?int {
    $key = $this->generatePageKey();
    $override = $this->get($key);

    // If no override, then check for a default override set on the node type.
    if ($override == NULL) {
      $entity = $this->getPageEntity();
      if ($entity && $entity instanceof NodeInterface) {
        $override = $entity->type->entity->getThirdPartySetting('regions_override', 'default');
      }
    }

    return (int) $override;
  }

  /**
   * {@inheritdoc}
   */
  public function generatePageKey(): ?string {
    // Use "front" for the home page regardless if it is an entity or view.
    if ($this->pathMatcher->isFrontPage()) {
      return 'front';
    }

    // Default the key to the page route name.
    $key = $this->routeMatch->getRouteName();

    // Get the page entity.
    $entity = $this->getPageEntity();
    if ($entity) {
      $entity_type = $entity->getEntityTypeId();
      $entity_id = $entity->id();

      // Return the "front" page if this entity is being used for it.
      $front_uri = $this->configFactory->get('system.site')->get('page.front');
      $regex = "/$entity_type(\D+)$entity_id$/";
      if (preg_match($regex, $front_uri)) {
        $key = 'front';
      }
      else {
        $key = $entity_type . '.' . $entity_id;
      }
    }

    return $key;
  }

  /**
   * Get the page entity for the current route.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The current entity or NULL if no entity is found for the page.
   */
  public function getPageEntity(): ?EntityInterface {
    $page_entity = NULL;

    // Get the page entity.
    $params = $this->routeMatch->getParameters();
    foreach ($params as $param_key => $entity) {
      if ($entity instanceof EntityInterface) {
        // Ignore revision params.
        if (preg_match('/_revision$/', $param_key)) {
          continue;
        }

        // Remove node previews for anything other than the full display.
        if ($param_key == 'node_preview' && $params->get('view_mode_id') !== 'full') {
          continue;
        }

        $page_entity = $entity;
        break;
      }
    }

    return $page_entity;
  }

}
