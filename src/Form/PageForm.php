<?php

namespace Drupal\regions_override\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\Cache\CacheTagsInvalidator;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\node\Entity\Node;
use Drupal\path_alias\AliasManagerInterface;
use Drupal\regions_override\PageOverrideInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Regions Override settings for this site.
 */
class PageForm extends FormBase {

  /**
   * The override key of the page being altered.
   *
   * @var string|null
   */
  protected ?string $key;

  /**
   * Constructs a \Drupal\regions_override\Form\PageForm object.
   *
   * @param \Drupal\regions_override\PageOverrideInterface $pageOverride
   *   The Regions Override Page service.
   * @param \Drupal\Core\Cache\CacheTagsInvalidator $cacheTagsInvalidator
   *   The Cache Tags Invalidator service.
   * @param \Drupal\path_alias\AliasManagerInterface $aliasManager
   *   The Path Alias Manager service.
   */
  public function __construct(
    protected PageOverrideInterface $pageOverride,
    protected CacheTagsInvalidator $cacheTagsInvalidator,
    protected AliasManagerInterface $aliasManager,
  ) {
    $this->key = $this->getRequest()->get('key');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('regions_override.page'),
      $container->get('cache_tags.invalidator'),
      $container->get('path_alias.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'regions_override_page';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Exit if the page key is not set in the query string.
    if (!isset($this->key)) {
      $form['error'] = [
        '#prefix' => '<div class="messages messages--error">',
        '#suffix' => '</div>',
        '#markup' => $this->t('Oops! the page you want to override cannot be determined.'),
      ];
      return $form;
    }

    $form['page_path'] = [
      '#markup' => '<strong>' . $this->t('Page path:') . '</strong> ' . Html::escape($this->getPathFromKey($this->key)),
    ];

    // Check if an entity page has a default set by the Entity Type.
    $default = $this->pageOverride->get($this->key) ?? $this->getDefault();

    $options = [
      '0' => t('Show all available regions'),
      '1' => t('No Sidebars'),
      '2' => t('Remove Body regions (Header and Footer remain)'),
      '3' => t('Remove all regions'),
    ];
    foreach ($options as $option_key => $option_val) {
      if ($option_key == $this->getDefault()) {
        $options[$option_key] = '<em>Default</em> - ' . $option_val;
      }
    }

    $form['regions_override'] = [
      '#type' => 'radios',
      '#title' => $this->t('Region Display Options'),
      '#description' => $this->t('Choose how the wrapping theme regions should be displayed for this page layout. Drupal\'s "Content" region will always remain.'),
      '#default_value' => $default,
      '#options' => $options,
    ];

    // By default, render the form using system-config-form.html.twig.
    $form['#theme'] = 'system_config_form';

    // Set the submit button actions.
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save configuration'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Save the form value to the key/value store.
    $value = $form_state->getValue('regions_override');
    if ($value !== NULL && $value !== $this->getDefault()) {
      $this->pageOverride->set($this->key, $value);
    }
    else {
      $this->pageOverride->delete($this->key);
    }

    // Invalidate cache for pages with regions_override:<key>.
    $this->cacheTagsInvalidator->invalidateTags(['regions_override:' . $this->key]);

    $this->messenger()->addStatus($this->t('The regions override options have been saved.'));
  }

  /**
   * Get the path of the page being overridden.
   *
   * @param string $key
   *   The key used to identify a page.
   *
   * @return string
   *   The path of the page to be affected.
   */
  protected function getPathFromKey($key): string {
    if ($key == 'front') {
      return '<front>';
    }

    $path = '';

    $url = Url::fromRoute($key);

    try {
      $path = $url->getInternalPath();
    }
    catch (\Throwable $th) {
      $parts = explode('.', $key);
      $path = $this->aliasManager->getAliasByPath("/{$parts[0]}/" . $parts[1]);
    }

    return $path;
  }

  /**
   * Get the default value from Entity Type or just show all regions.
   *
   * @return string
   *   The default value.
   */
  protected function getDefault(): string {
    // Assume a default set to all regions.
    $type_default = '0';
    // Check if an entity page has a default set by the Entity Type.
    $path = $this->getRequest()->get('destination');

    if (!$path) {
      return $type_default;
    }

    $url = Url::fromUserInput('/' . $path);
    $routed = $url->isRouted();
    if ($routed) {
      $params = $url->getRouteParameters();
    }

    if (isset($params['node'])) {
      $node = Node::load($params['node']);
      $entity_override = $node->type->entity->getThirdPartySetting('regions_override', 'default');

      if (!empty($entity_override)) {
        $type_default = $entity_override;
      }
    }

    return $type_default;
  }

}
