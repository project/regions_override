<?php

namespace Drupal\regions_override\FormAlters;

use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\regions_override\OverrideUtility;
use Drupal\regions_override\PageOverrideInterface;

/**
 * Provides functions for altering node forms.
 */
class NodeForm {

  use DependencySerializationTrait;

  /**
   * Constructs a NodeForm object.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $accountProxy
   *   The account proxy service.
   * @param \Drupal\regions_override\PageOverrideInterface $pageOverride
   *   The page override service.
   * @param \Drupal\regions_override\OverrideUtility $overrideUtility
   *   The override utility service.
   */
  public function __construct(
    protected AccountProxyInterface $accountProxy,
    protected PageOverrideInterface $pageOverride,
    protected OverrideUtility $overrideUtility,
  ) {
  }

  /**
   * Alter the Node Edit form.
   *
   * Use with hook_form_node_form_alter().
   *
   * @param array $form
   *   The form being altered.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @see \Drupal\node\NodeForm::form()
   */
  public function addOverrideOptionsToForm(array &$form, FormStateInterface $form_state): void {
    if (!$form_state->get('form_display')?->getComponent('regions_override')
      || !$this->accountProxy->hasPermission('override regions')) {
      return;
    }

    /** @var \Drupal\node\Entity\Node $node */
    $node = $form_state->getFormObject()->getEntity();
    $type_default = $node->type->entity->getThirdPartySetting('regions_override', 'default', 0);
    $key = NULL;
    if ($node->id()) {
      $key = $node->getEntityTypeId() . '.' . $node->id();
      if ($this->overrideUtility->nodeIsFront($node->id())) {
        $key = 'front';
      }
    }

    $form['regions_override'] = [
      '#type' => 'fieldset',
      '#title' => t('Regions Override'),
    ];

    $default = $this->pageOverride->get($key) ?? $type_default;

    $options = [
      '0' => t('Show all available regions'),
      '1' => t('No Sidebars'),
      '2' => t('Remove Body regions (Header and Footer remain)'),
      '3' => t('Remove all regions'),
    ];
    foreach ($options as $option_key => $option_val) {
      if ($option_key == $type_default) {
        $options[$option_key] = '<em>Default</em> - ' . $option_val;
      }
    }

    $form['regions_override']['regions_override_options'] = [
      '#type' => 'radios',
      '#title' => t('Region Display Options'),
      '#description' => t('Choose how the wrapping theme regions should be displayed for this page layout. Drupal\'s "Content" region will always remain. NOTICE: This change will not display on a node Preview screen if not yet saved.'),
      '#default_value' => $default,
      '#options' => $options,
    ];

    // Add a custom submit handler to save override data.
    $form['actions']['submit']['#submit'][] = [$this, 'submitHandler'];
  }

  /**
   * Callback to save regions_override settings for a specific node.
   *
   * @param array $form
   *   Form being submitted.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Current form state.
   *
   * @see \Drupal\node\NodeForm::submitForm()
   */
  public function submitHandler(array $form, FormStateInterface $form_state): void {
    /** @var \Drupal\node\Entity\Node $node */
    $node = $form_state->getFormObject()->getEntity();
    $type_default = $node->type->entity->getThirdPartySetting('regions_override', 'default', 0);

    $nid = $form_state->getValue('nid');
    $key = 'node.' . $nid;
    if ($this->overrideUtility->nodeIsFront($nid)) {
      $key = 'front';
    }
    $value = $form_state->getValue('regions_override_options');

    if (isset($value) && $value !== $type_default) {
      $this->pageOverride->set($key, $value);
    }
    else {
      $this->pageOverride->delete($key);
    }
  }

}
