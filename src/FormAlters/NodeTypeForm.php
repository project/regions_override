<?php

namespace Drupal\regions_override\FormAlters;

use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\NodeType;
use Drupal\node\NodeTypeInterface;

/**
 * Provides functions for altering node type forms.
 */
class NodeTypeForm {

  use DependencySerializationTrait;

  /**
   * Adds regions override options to the node type form.
   *
   * Use in hook_form_node_type_form_alter().
   *
   * @param array $form
   *   The current form being altered.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @see \Drupal\node\NodeTypeForm::form()
   * @see \Drupal\node\NodeTypeForm::buildEntity()
   */
  public function addOverrideOptionsToForm(array &$form, FormStateInterface $form_state): void {
    /** @var \Drupal\node\NodeTypeInterface $type */
    $type = $form_state->getFormObject()->getEntity();
    $form['regions_override'] = [
      '#type' => 'details',
      '#title' => t('Regions Override'),
      '#group' => 'additional_settings',
    ];
    $form['regions_override']['regions_override_default'] = [
      '#type' => 'radios',
      '#title' => t('Region Display Options'),
      '#description' => t('Choose how the wrapping theme regions should be displayed for this content type.'),
      '#default_value' => $type->getThirdPartySetting('regions_override', 'default', 0),
      '#options' => [
        '0' => t('Show All Available Regions'),
        '1' => t('No Sidebars'),
        '2' => t('Remove Content Regions (header and footer remain)'),
        '3' => t('Remove all regions'),
      ],
    ];

    $form['#entity_builders'][] = [$this, 'formBuilderCallback'];
  }

  /**
   * Entity builder callback for the submitted node type form.
   *
   * @param string $entity_type
   *   The entity type id.
   * @param \Drupal\node\NodeTypeInterface $type
   *   The node type entity.
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @see \Drupal\node\NodeTypeForm::buildEntity()
   */
  public function formBuilderCallback(string $entity_type, NodeTypeInterface $type, array &$form, FormStateInterface $form_state): void {
    $value = $form_state->getValue('regions_override_default');
    if (!empty($value)) {
      $type->setThirdPartySetting('regions_override', 'default', $value);
    }
    else {
      $type->unsetThirdPartySetting('regions_override', 'default');
    }
  }

  /**
   * Create a Pseudo Field for regions override options.
   *
   * Use with hook_entity_extra_field_info().
   *
   * @return array
   *   Extra field info.
   */
  public function addPseudoField(): array {
    $extra = [];

    foreach (NodeType::loadMultiple() as $bundle) {
      $extra['node'][$bundle->id()]['form']['regions_override'] = [
        'label' => t('Regions Override'),
        'description' => t('Allow changing the regions displayed on the default display.'),
        'weight' => 100,
      ];
    }

    return $extra;
  }

}
