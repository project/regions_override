<?php

namespace Drupal\regions_override\FormAlters;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Form\FormStateInterface;
use Drupal\regions_override\OverrideUtility;

/**
 * Provides functions for altering theme settings form.
 */
class ThemeSettingsForm {

  use DependencySerializationTrait;

  /**
   * ThemeSettingsForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\regions_override\OverrideUtility $overrideUtility
   *   The override utility.
   */
  public function __construct(
    protected ConfigFactoryInterface $configFactory,
    protected OverrideUtility $overrideUtility,
  ) {
  }

  /**
   * Add region override settings into a theme settings form.
   *
   * Use with Implements hook_form_system_theme_settings_alter().
   *
   * @param array $form
   *   The form being altered.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @see \Drupal\system\Form\ThemeSettingsForm::buildForm()
   */
  public function addSettings(array &$form, FormStateInterface $form_state): void {
    $theme = $this->getThemeBeingEdited($form_state);
    if (!$theme) {
      return;
    }

    // Get the theme config for the current theme.
    $config = $this->overrideUtility->getThemeConfig($theme);
    $all_regions = $this->overrideUtility->getThemeRegions($theme, TRUE);

    // Preserve a settings if previously set in a theme.
    $previous_settings = $form['regions_override'] ?? NULL;

    $form['regions_override'] = [
      '#type' => 'details',
      '#title' => t('Regions Override'),
      '#description' => t('Choose the regions that belong in each section of a page which can be overridden on a page by page basis.'),
      '#group' => $previous_settings['#group'] ?? NULL,
      '#open' => $previous_settings['#open'] ?? TRUE,
      '#attached' => [
        'library' => [
          'regions_override/regions-override',
        ],
      ],
    ];

    $form['regions_override']['wrapper'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['regions-override-sections-wrapper']],
    ];

    $form['regions_override']['wrapper']['header'] = [
      '#type' => 'checkboxes',
      '#title' => t('Header Regions'),
      '#options' => $all_regions,
      '#default_value' => $config->get('header') ?? [],
    ];
    $form['regions_override']['wrapper']['sidebar'] = [
      '#type' => 'checkboxes',
      '#title' => t('Sidebar Regions'),
      '#options' => $all_regions,
      '#default_value' => $config->get('sidebar') ?? [],
    ];
    $form['regions_override']['wrapper']['footer'] = [
      '#type' => 'checkboxes',
      '#title' => t('Footer Regions'),
      '#options' => $all_regions,
      '#default_value' => $config->get('footer') ?? [],
    ];

    // Ensure the submission happens first so we can unset extra values.
    array_unshift($form['#submit'], [$this, 'submitHandler']);
  }

  /**
   * Form submission handler for theme_settings_alter.
   *
   * @param array $form
   *   The form being submitted.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @see \Drupal\system\Form\ThemeSettingsForm::submitForm()
   */
  public function submitHandler(array $form, FormStateInterface $form_state): void {
    $theme = $this->getThemeBeingEdited($form_state);
    if (!$theme) {
      return;
    }

    // Get the theme config for the current theme.
    $config = $this->configFactory
      ->getEditable('regions_override.theme.' . $theme);

    $override_settings = ['header', 'sidebar', 'footer'];
    foreach ($override_settings as $setting_name) {
      $values = $form_state->getValue($setting_name) ?? [];
      $values = array_filter($values, function ($value) {
        return $value !== 0;
      });

      $config->set($setting_name, array_keys($values));
      // Avoid override settings spilling over to theme settings.
      $form_state->unsetValue($setting_name);
    }
    $config->save();
  }

  /**
   * Return the theme currently being edited.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return string|null
   *   The theme being edited or NULL if not found.
   */
  protected function getThemeBeingEdited(FormStateInterface $form_state): ?string {
    $build_info = $form_state->getBuildInfo();
    return $build_info['args'][0] ?? NULL;
  }

}
