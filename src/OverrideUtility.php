<?php

namespace Drupal\regions_override;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Routing\AdminContext;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Theme\ThemeManagerInterface;
use Drupal\Core\Url;

/**
 * Provides utility functions for rendering.
 */
class OverrideUtility {

  /**
   * Constructs a OverrideUtility object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   * @param \Drupal\Core\Routing\AdminContext $adminContext
   *   The admin context service.
   * @param \Drupal\Core\Session\AccountProxyInterface $accountProxy
   *   The account proxy service.
   * @param \Drupal\Core\Theme\ThemeManagerInterface $themeManager
   *   The theme manager service.
   * @param \Drupal\Core\Extension\ThemeHandlerInterface $themeHandler
   *   The theme handler service.
   * @param \Drupal\regions_override\PageOverrideInterface $pageOverride
   *   The page override service.
   */
  public function __construct(
    protected ConfigFactoryInterface $configFactory,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected AdminContext $adminContext,
    protected AccountProxyInterface $accountProxy,
    protected ThemeManagerInterface $themeManager,
    protected ThemeHandlerInterface $themeHandler,
    protected PageOverrideInterface $pageOverride,
  ) {
  }

  /**
   * Preprocesses the Html template variables.
   *
   * @param array $variables
   *   The html wrapper variables.
   */
  public function preprocessHtml(&$variables) {
    // Ignore Admin routes.
    if ($this->isAdminRoute()) {
      return;
    }

    $key = $this->pageOverride->generatePageKey();
    if (empty($key)) {
      return;
    }

    $variables['#cache']['tags'][] = 'regions_override:' . $key;
    $page_override = $this->pageOverride->getOverride();

    if (empty($page_override)) {
      return;
    }

    // 0 - Default
    // 1 - No Sidebars
    // 2 - Remove Content Regions (header and footer remain)
    // 3 - Remove all regions
    switch ($page_override) {
      case "1":
        $regions_to_remove = $this->getSectionRegions('sidebar');
        $this->removeRegion($regions_to_remove, $variables);
        $variables['page']['#region_type'] = 'sidebars';
        break;

      case "2":
        // Get the regions we want to keep.
        $headers = $this->getSectionRegions('header');
        $footers = $this->getSectionRegions('footer');
        $regions_to_keep = array_merge($headers, $footers);
        // From all theme's regions filter out the regions we want to keep.
        $filtered_regions = array_diff($this->getThemeRegions(), $regions_to_keep);
        $this->removeRegion($filtered_regions, $variables);
        $variables['page']['#region_type'] = 'body';
        break;

      case "3":
        $this->removeRegion($this->getThemeRegions(), $variables);
        $variables['page']['#region_type'] = 'all';
        break;
    }

  }

  /**
   * Add a button into the admin toolbar that will override the current page.
   *
   * Used with the hook_toolbar() implementation.
   *
   * @return array
   *   The toolbar items, keyed by unique identifiers
   */
  public function addToolbarButton(): array {
    // Ignore Admin routes.
    if ($this->isAdminRoute()) {
      return [];
    }

    $query['key'] = $this->pageOverride->generatePageKey();
    if (empty($query['key'])) {
      return [];
    }
    $query['destination'] = Url::fromRoute('<current>')->getInternalPath();

    // The 'Regions Override' tab is a simple link, with no corresponding tray.
    $items['regions_override'] = [
      '#cache' => [
        'contexts' => ['user.permissions', 'url.path'],
      ],
      '#type' => 'toolbar_item',
      'tab' => [
        '#access' => $this->accountProxy->hasPermission('override regions'),
        '#type' => 'link',
        '#title' => t('Regions Override'),
        '#url' => Url::fromRoute('regions_override.page_form', [], ['query' => $query]),
        '#attributes' => [
          'title' => t('Remove regions from the current page'),
          'class' => ['toolbar-icon', 'toolbar-icon-regions-override'],
        ],
      ],
      '#wrapper_attributes' => [
        'class' => ['toolbar-tab', 'regions-override-toolbar-tab'],
      ],
      '#attached' => [
        'library' => [
          'regions_override/regions-override',
        ],
      ],
      '#weight' => 100,
    ];

    return $items;
  }

  /**
   * Get the regions_override config for the current active or a given theme.
   *
   * @param string|null $theme_name
   *   The machine name of a theme.
   *
   * @return \Drupal\Core\Config\ImmutableConfig
   *   Config for a region override for the given theme.
   */
  public function getThemeConfig(?string $theme_name = NULL): ImmutableConfig {
    if (!$theme_name) {
      $theme_name = $this->getThemeName();
    }

    $config = $this->configFactory->get('regions_override.theme.' . $theme_name);
    // Fallback to the base theme's config if not found in the active theme.
    if ($config->get('header') === NULL) {
      $base_theme = $this->themeHandler->listInfo()[$theme_name]?->base_theme ?? NULL;
      if ($base_theme) {
        $config = $this->configFactory->get('regions_override.theme.' . $base_theme);
      }
    }
    return $config;
  }

  /**
   * Check if a given node id is the front page.
   *
   * @param int|string $nid
   *   The Node ID to check to see if it is the front page.
   */
  public function nodeIsFront(int|string $nid): bool {
    $config = $this->configFactory->get('system.site');
    $front_uri = $config->get('page.front');
    preg_match('/node\/(\d+)/', $front_uri, $matches);
    return !empty($matches[1]) && $matches[1] == $nid;
  }

  /**
   * Remove regions from the page, but always keep the Content region.
   *
   * @param array $regions
   *   List of all regions by machine name.
   * @param array $variables
   *   Available theme variables.
   */
  public function removeRegion(array $regions, array &$variables): void {
    // Prevent the Content region from being removed.
    $filtered_regions = array_diff($regions, ['content']);
    $this->preserveLocalTasksBlock($filtered_regions, $variables);

    foreach ($filtered_regions as $region) {
      if (isset($variables['page'][$region])) {
        unset($variables['page'][$region]);
      }
    }

    $variables['page']['#regions_overridden'] = TRUE;
  }

  /**
   * Prevent the Local Tasks Block from being removed.
   *
   * @param array $all_regions
   *   List of all the region machine names.
   * @param array $variables
   *   List or theme variables.
   */
  protected function preserveLocalTasksBlock(array &$all_regions, array &$variables): void {
    foreach ($all_regions as $region_key => $region) {
      $block_id = $this->regionHasLocalTasks($region);
      if ($block_id) {
        // Remove everything that is not local_tasks and does not start with #.
        foreach ($variables['page'][$region] as $key => $block) {
          if (!str_starts_with($key, '#') && $key !== $block_id) {
            unset($variables['page'][$region][$key]);
          }
        }

        // Remove this region from possible removal.
        unset($all_regions[$region_key]);
      }
    }
  }

  /**
   * Detects if region has local_tasks_block.
   *
   * @param string $region
   *   The region machine name to check against.
   *
   * @return bool|string
   *   False if there is nothing, or block machine name if it finds
   *   local_tasks_block.
   */
  protected function regionHasLocalTasks(string $region): bool|string {
    // Don't remove this region if "local_tasks_block" is inside it.
    $blocks = $this->entityTypeManager
      ->getStorage('block')->loadByProperties([
        'theme' => $this->getThemeName(),
        'region' => $region,
      ]);

    foreach ($blocks as $block) {
      if ($block->getPluginId() == 'local_tasks_block') {
        return $block->id();
      }
    }
    return FALSE;
  }

  /**
   * Get the Active Theme's name.
   *
   * @return string
   *   The machine name of the active theme.
   */
  protected function getThemeName(): string {
    return $this->themeManager->getActiveTheme()->getName();
  }

  /**
   * Get the regions for the active theme or a given theme.
   *
   * @param string|null $theme
   *   The machine name of a theme.
   * @param bool $withLabel
   *   Whether to return the regions with their labels for a given theme.
   *
   * @return array
   *   An array of regions.
   */
  public function getThemeRegions(?string $theme = NULL, bool $withLabel = FALSE): array {
    if (!$theme) {
      return $this->themeManager->getActiveTheme()->getRegions();
    }

    $regions = $this->themeHandler->listInfo()[$theme]?->info['regions'] ?? [];
    return (!$withLabel) ? array_keys($regions) : $regions;
  }

  /**
   * Determine if the current route is an admin route.
   *
   * @return bool
   *   Whether the current route is an admin route.
   */
  protected function isAdminRoute(): bool {
    return $this->adminContext->isAdminRoute();
  }

  /**
   * Get the regions assigned to a given section.
   *
   * @param string $section
   *   The section of regions. Possible values: 'header', 'sidebar', 'footer'.
   *
   * @return array
   *   List of regions for the given section.
   */
  protected function getSectionRegions(string $section): array {
    // Exit early if the section is not valid.
    $possible_sections = ['header', 'sidebar', 'footer'];
    if (!in_array($section, $possible_sections)) {
      return [];
    }

    $config = $this->getThemeConfig();
    $theme_has_config = !empty($config->getRawData());

    // If the theme has a config for the section, return it.
    if ($theme_has_config && $config->get($section) !== NULL) {
      return $config->get($section) ?? [];
    }

    // If the theme does not have a config for the section, try to set defaults.
    $possible_regions = match($section) {
      'header' => ['header', 'primary_menu', 'secondary_menu'],
      'sidebar' => ['sidebar', 'sidebar_first', 'sidebar_second'],
      'footer' => ['footer', 'footer_top', 'footer_bottom'],
    };

    $all_regions = $this->getThemeRegions();
    $default_regions = [];
    foreach ($possible_regions as $region) {
      if (in_array($region, $all_regions)) {
        $default_regions[] = $region;
      }
    }

    return $default_regions;
  }

  /**
   * Add page theme suggestions based on the override type.
   *
   * Used with the hook_theme_suggestions_HOOK_alter().
   *
   * @param array $suggestions
   *   An array of suggestions.
   * @param array $variables
   *   An array of variables.
   */
  public function setThemeSuggestions(array &$suggestions, array &$variables): void {
    if (empty($variables['page']['#regions_overridden'])) {
      return;
    }

    $override_type = $variables['page']['#region_type'];

    // Add the general region type suggestion as the first suggestion.
    array_unshift($suggestions, 'page__override_' . $override_type);

    $entity = $this->pageOverride->getPageEntity();
    if ($entity instanceof EntityInterface) {
      $bundle = $entity->bundle();
      $entity_type = $entity->getEntityTypeId();

      // Change taxonomy to match the way drupal normally formats it.
      if ($entity_type == 'taxonomy_term') {
        $entity_type = 'taxonomy__term';
      }

      $entity_suggestions = [
        'page__override_' . $override_type . '__' . $entity_type,
        'page__override_' . $override_type . '__' . $entity_type . '__' . $bundle,
      ];

      // Insert the node suggestions after the 'page__node' suggestion.
      $key = array_search('page__' . $entity_type, $suggestions);
      array_splice($suggestions, $key + 1, 0, $entity_suggestions);

      // If page__front is a suggestion, put the override suggestion before it.
      $eid_suggestion = 'page__override_' . $override_type . '__' . $entity_type . '__' . $entity->id();
      if (($key = array_search('page__front', $suggestions)) !== FALSE) {
        array_splice($suggestions, $key, 0, $eid_suggestion);
      }
      else {
        $suggestions[] = $eid_suggestion;
      }
    }
  }

}
