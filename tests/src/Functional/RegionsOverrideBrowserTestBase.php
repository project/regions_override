<?php

namespace Drupal\Tests\regions_override\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\node\Traits\ContentTypeCreationTrait;

/**
 * A standardized base class for Functional tests in Regions Override.
 */
abstract class RegionsOverrideBrowserTestBase extends BrowserTestBase {

  use ContentTypeCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'block',
    'regions_override',
    'toolbar',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * A user with permission to edit nodes and region overrides.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * The seeded node page.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $nodePage;

  /**
   * The body field of the seeded node page.
   *
   * @var string
   */
  protected $nodePageBody = 'Test body';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->user = $this->drupalCreateUser([
      'access administration pages',
      'access toolbar',
      'administer blocks',
      'administer themes',
      'override regions',
      'bypass node access',
    ]);
    $this->drupalLogin($this->user);

    // Set the default theme to the test theme.
    $this->assertTrue(\Drupal::service('theme_installer')
      ->install(['test_regions_override_theme']));
    $this->config('system.theme')
      ->set('default', 'test_regions_override_theme')
      ->save();

    $this->createContentType(['type' => 'page', 'name' => 'Page']);
    $this->seedPage();
    $this->seedBlocks();
  }

  /**
   * Add blocks to the header, sidebars and footer regions.
   */
  protected function seedBlocks(): void {
    // Add a block to the header, sidebars and footer regions.
    $this->drupalPlaceBlock('system_powered_by_block', [
      'id' => 'powered_by_header',
      'region' => 'header',
    ]);
    $this->drupalPlaceBlock('system_powered_by_block', [
      'id' => 'powered_by_first',
      'region' => 'sidebar_first',
    ]);
    $this->drupalPlaceBlock('system_powered_by_block', [
      'id' => 'powered_by_second',
      'region' => 'sidebar_second',
    ]);
    $this->drupalPlaceBlock('system_powered_by_block', [
      'id' => 'powered_by_footer',
      'region' => 'footer',
    ]);
    $this->drupalPlaceBlock('system_powered_by_block', [
      'id' => 'powered_by_highlighted',
      'region' => 'highlighted',
    ]);
    // Add the primary tabs block to the help region.
    $this->drupalPlaceBlock('local_tasks_block', [
      'id' => 'primary_tabs',
      'region' => 'help',
    ]);
  }

  /**
   * Set the regions override configuration for a theme.
   *
   * @param string $theme_name
   *   The machine name of the theme.
   */
  protected function setThemeOverrideConfig(string $theme_name = 'test_regions_override_theme'): void {
    $this->config('regions_override.theme.' . $theme_name)
      ->set('header', [
        'header',
      ])
      ->set('sidebar', [
        'sidebar_first',
        'sidebar_second',
      ])
      ->set('footer', [
        'footer',
      ])
      ->save();
  }

  /**
   * Create a basic page node.
   */
  protected function seedPage(): void {
    $this->nodePage = $this->drupalCreateNode([
      'type' => 'page',
      'title' => 'Test Page',
      'body' => [['value' => $this->nodePageBody]],
    ]);
  }

  /**
   * Confirm that the seeded test page loads.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Behat\Mink\Exception\ResponseTextException
   */
  protected function assertTestPageLoads(): void {
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains($this->nodePageBody);
  }

  /**
   * Assert that all regions with our seeded blocks render on the page.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   */
  protected function assertAllRegionsRender(): void {
    $assert = $this->assertSession();
    $assert->elementExists('css', '#header');
    $assert->elementExists('css', '#footer');
    $assert->elementExists('css', '.layout-content');
    $assert->elementExists('css', '.layout-sidebar-first');
    $assert->elementExists('css', '.layout-sidebar-second');
    $assert->elementExists('css', '#block-powered-by-highlighted');
    $assert->elementExists('css', '#block-primary-tabs');
  }

  /**
   * Assert that sidebars are not rendered on the page.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   */
  protected function assertSidebarRegionsHidden(): void {
    $assert = $this->assertSession();
    $assert->elementExists('css', '#header');
    $assert->elementExists('css', '#footer');
    $assert->elementExists('css', '.layout-content');
    $assert->elementExists('css', '#block-powered-by-highlighted');
    $assert->elementExists('css', '#block-primary-tabs');

    // Sidebars should be hidden.
    $assert->elementNotExists('css', '.layout-sidebar-first');
    $assert->elementNotExists('css', '.layout-sidebar-second');
  }

  /**
   * Ensure body regions but the content and the primary tabs block are hidden.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   */
  protected function assertBodyRegionsHidden(): void {
    $assert = $this->assertSession();

    $assert->elementExists('css', '#header');
    $assert->elementExists('css', '#footer');
    $assert->elementExists('css', '.layout-content');
    $assert->elementExists('css', '#block-primary-tabs');

    // Body content regions should be hidden.
    $assert->elementNotExists('css', '.layout-sidebar-first');
    $assert->elementNotExists('css', '.layout-sidebar-second');
    $assert->elementNotExists('css', '#block-powered-by-highlighted');
  }

  /**
   * Ensure all regions but the content and the primary tabs block are hidden.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   */
  protected function assertAllRegionsHidden(): void {
    $assert = $this->assertSession();

    $assert->elementNotExists('css', '#header');
    $assert->elementNotExists('css', '#footer');
    $assert->elementNotExists('css', '.layout-sidebar-first');
    $assert->elementNotExists('css', '.layout-sidebar-second');
    $assert->elementNotExists('css', '#block-powered-by-highlighted');

    // The content and primary tabs block should still be rendered.
    $assert->elementExists('css', '.layout-content');
    $assert->elementExists('css', '#block-primary-tabs');
  }

}
