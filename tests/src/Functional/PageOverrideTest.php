<?php

namespace Drupal\Tests\regions_override\Functional;

/**
 * Tests that the overridden layout is displayed when the page is viewed.
 *
 * @group regions_override
 */
class PageOverrideTest extends RegionsOverrideBrowserTestBase {

  /**
   * Tests overridden sidebar layout is displayed when the page is viewed.
   */
  public function testPageOverrideSidebars() {
    // View the basic page node and confirm that all regions are rendered.
    $this->drupalGet('node/' . $this->nodePage->id());
    $this->assertSession()->pageTextContains('Regions Override');
    $this->assertAllRegionsRender();

    // Edit the basic page node and remove sidebars.
    $this->drupalGet('node/' . $this->nodePage->id() . '/edit');
    $this->submitForm([
      'regions_override_options' => '1',
    ], 'Save');

    // Confirm that the sidebars are no longer rendered.
    $this->assertTestPageLoads();
    $this->assertSidebarRegionsHidden();
  }

  /**
   * Tests that Body regions are removed when the page is overridden.
   */
  public function testPageOverrideBody() {
    // View the basic page node and confirm that all regions are rendered.
    $this->drupalGet('node/' . $this->nodePage->id());
    $this->assertAllRegionsRender();

    // Edit the basic page node and remove body regions.
    $this->drupalGet('node/' . $this->nodePage->id() . '/edit');
    $this->submitForm([
      'regions_override_options' => '2',
    ], 'Save');

    // Confirm that the body regions are no longer rendered.
    $this->assertTestPageLoads();
    $this->assertBodyRegionsHidden();
  }

  /**
   * Test that the front page can be overridden.
   */
  public function testFrontPageOverride() {
    // Set the front page to the test page.
    $this->config('system.site')
      ->set('page.front', '/node/' . $this->nodePage->id())
      ->save();

    // View the front page.
    $this->drupalGet('node/' . $this->nodePage->id());
    $this->assertTestPageLoads();
    $this->assertAllRegionsRender();

    // Click the link in the toolbar to edit the node's region overrides.
    $this->clickLink('Regions Override');
    $this->assertSession()->pageTextContains('Page path: <front>');

    // Select the "Remove All Regions" option.
    $this->submitForm([
      'regions_override' => '3',
    ], 'Save configuration');

    $this->assertTestPageLoads();
    $this->assertAllRegionsHidden();
  }

  /**
   * Tests that a theme can determine which regions are mapped to each section.
   */
  public function testThemeCustomConfig() {
    // Set the theme configuration to map regions to sections.
    $this->drupalGet('admin/appearance/settings/test_regions_override_theme');
    $this->submitForm([
      'header[header]' => TRUE,
      'sidebar[sidebar_first]' => TRUE,
      'sidebar[sidebar_second]' => TRUE,
      'footer[footer]' => TRUE,
    ], 'Save configuration');
    $this->assertSession()->pageTextContains('The configuration options have been saved.');

    // View the basic page node and confirm that all regions are rendered.
    $this->drupalGet('node/' . $this->nodePage->id());
    $this->assertAllRegionsRender();

    // Remove sidebars.
    $this->drupalGet('node/' . $this->nodePage->id() . '/edit');
    $this->submitForm(['regions_override_options' => '1'], 'Save');
    $this->assertTestPageLoads();
    $this->assertSidebarRegionsHidden();

    // Remove body regions.
    $this->drupalGet('node/' . $this->nodePage->id() . '/edit');
    $this->submitForm(['regions_override_options' => '2'], 'Save');
    $this->assertTestPageLoads();
    $this->assertBodyRegionsHidden();

    // Remove the theme configuration so that all regions are the same.
    $this->drupalGet('admin/appearance/settings/test_regions_override_theme');
    $this->submitForm([
      'header[header]' => FALSE,
      'sidebar[sidebar_first]' => FALSE,
      'sidebar[sidebar_second]' => FALSE,
      'footer[footer]' => FALSE,
    ], 'Save configuration');
    $this->assertSession()->pageTextContains('The configuration options have been saved.');

    // Remove sidebars (which should still render).
    $this->drupalGet('node/' . $this->nodePage->id() . '/edit');
    $this->submitForm(['regions_override_options' => '1'], 'Save');
    $this->assertTestPageLoads();
    $this->assertAllRegionsRender();

    // Remove body regions (which should remove all regions).
    $this->drupalGet('node/' . $this->nodePage->id() . '/edit');
    $this->submitForm(['regions_override_options' => '2'], 'Save');
    $this->assertTestPageLoads();
    $this->assertAllRegionsHidden();
  }

}
