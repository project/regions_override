<?php

namespace Drupal\Tests\regions_override\Unit;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\Core\KeyValueStore\KeyValueStoreInterface;
use Drupal\Core\Path\PathMatcherInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\node\NodeInterface;
use Drupal\regions_override\PageOverride;
use Drupal\Tests\UnitTestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use Symfony\Component\HttpFoundation\ParameterBag;

/**
 * @coversDefaultClass \Drupal\regions_override\PageOverride
 * @group regions_override
 */
class PageOverrideTest extends UnitTestCase {

  use ProphecyTrait;

  /**
   * The mocked key value store.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  protected $keyValueStore;

  /**
   * The mocked currently active route match object.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The mocked path matcher service.
   *
   * @var \Drupal\Core\Path\PathMatcherInterface
   */
  protected $pathMatcher;

  /**
   * The Config Factory service stub.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Instance of the PageOverride.
   *
   * @var \Drupal\regions_override\PageOverride
   */
  protected $classInstance;

  /**
   * Create the setup for constants and configFactory stub.
   */
  protected function setUp(): void {
    parent::setUp();

    // Mock the Key Value store.
    $this->keyValueStore = $this->prophesize(KeyValueStoreInterface::class);

    $keyValueFactory = $this->prophesize(KeyValueFactoryInterface::class);
    $keyValueFactory->get('regions_override')
      ->willReturn($this->keyValueStore->reveal());

    // Mock the Route Match.
    $this->routeMatch = $this->prophesize(RouteMatchInterface::class);
    $this->routeMatch->getRouteName()->willReturn('view.test.page_1');
    $this->routeMatch->getParameters()->willReturn([]);

    $this->pathMatcher = $this->prophesize(PathMatcherInterface::class);
    $this->pathMatcher->isFrontPage()->willReturn(FALSE);

    // Stub config.
    $this->configFactory = $this->getConfigFactoryStub([
      'system.site' => [
        'page.front' => 'node.3',
      ],
    ]);

    $this->classInstance = new PageOverride(
      $keyValueFactory->reveal(),
      $this->routeMatch->reveal(),
      $this->pathMatcher->reveal(),
      $this->configFactory,
    );
  }

  /**
   * Tests the get() method.
   *
   * @see ::get()
   */
  public function testGet() {
    $this->keyValueStore->get('node.1')
      ->willReturn('2')
      ->shouldBeCalled();
    $value = $this->classInstance->get('node.1');

    $this->assertEquals('2', $value);
  }

  /**
   * Tests the get() method that returns no value and will cache NULL.
   *
   * @see ::get()
   */
  public function testGetNull() {
    $this->keyValueStore->get('node.1')
      ->willReturn(NULL)
      ->shouldBeCalled();
    $value = $this->classInstance->get('node.1');

    $this->assertNull($value);
    $this->assertEquals(['node.1' => NULL], $this->getCache());
  }

  /**
   * Tests the get() method returning a cached value.
   *
   * @see ::get()
   */
  public function testGetCached() {
    // Seed the cache.
    $this->setCache(['view.test.page_1' => '1', 'node.1' => '2']);

    $this->keyValueStore->get('node.1')->shouldNotBeCalled();
    $value = $this->classInstance->get('node.1');

    $this->assertEquals('2', $value);
  }

  /**
   * Tests the get() method returning a NULL cached value.
   *
   * @see ::get()
   */
  public function testGetCachedNull() {
    // Seed the cache.
    $this->setCache(['view.test.page_1' => '1', 'node.1' => NULL]);

    $this->keyValueStore->get('node.1')->shouldNotBeCalled();
    $value = $this->classInstance->get('node.1');

    $this->assertNull($value);
  }

  /**
   * Tests the set() method.
   *
   * @see ::set()
   */
  public function testSet() {
    $this->keyValueStore->set('node.1', '2')->shouldBeCalled();
    $this->classInstance->set('node.1', '2');

    $this->assertEquals(['node.1' => '2'], $this->getCache());
  }

  /**
   * Tests the delete() method.
   *
   * @see ::delete()
   */
  public function testDelete() {
    // Seed the cache.
    $this->setCache(['view.test.page_1' => '1', 'node.1' => '2']);

    $this->keyValueStore->delete('node.1')->shouldBeCalled();
    $this->classInstance->delete('node.1');

    $this->assertEquals(['view.test.page_1' => '1'], $this->getCache());
  }

  /**
   * Tests the resetCache() method.
   *
   * @see ::resetCache()
   */
  public function testResetCache() {
    // Seed the cache.
    $this->setCache(['view.test.page_1' => '1', 'node.1' => '2']);

    $this->classInstance->resetCache();

    $this->assertEquals([], $this->getCache());
  }

  /**
   * Tests the getOverride() method.
   *
   * @see ::getOverride()
   */
  public function testGetOverride() {
    $this->keyValueStore->get('view.test.page_1')->willReturn('1');

    $value = $this->classInstance->getOverride();

    $this->assertEquals('1', $value);
  }

  /**
   * Tests the generatePageKey() method.
   *
   * @see ::generatePageKey()
   */
  public function testGeneratePageKey() {
    $key = $this->classInstance->generatePageKey();

    $this->assertEquals('view.test.page_1', $key);
  }

  /**
   * Tests the generatePageKey() method when set as front.
   *
   * @see ::generatePageKey()
   */
  public function testGenerateFrontPageKey() {
    $this->pathMatcher->isFrontPage()->willReturn(TRUE);

    $key = $this->classInstance->generatePageKey();

    $this->assertEquals('front', $key);
  }

  /**
   * Tests the generatePageKey() method as a raw entity set as front.
   *
   * @see ::generatePageKey()
   */
  public function testGenerateFrontPageKeyAsEntity() {
    $this->pathMatcher->isFrontPage()->willReturn(FALSE);

    $node = $this->prophesize(NodeInterface::class);
    $node->getEntityTypeId()->willReturn('node');
    $node->id()->willReturn('3');

    $params = new ParameterBag([
      'node' => $node->reveal(),
    ]);
    $this->routeMatch->getParameters()->willReturn($params);

    $key = $this->classInstance->generatePageKey();

    $this->assertEquals('front', $key);
  }

  /**
   * Tests the generatePageKey() method for a node entity page.
   *
   * @see ::generatePageKey()
   */
  public function testGeneratePageKeyForEntity() {
    $node = $this->prophesize(NodeInterface::class);
    $node->getEntityTypeId()->willReturn('node');
    $node->id()->willReturn('1');

    $ignoredEntity = $this->prophesize(EntityInterface::class);

    $params = new ParameterBag([
      'node_revision' => $ignoredEntity->reveal(),
      'node_preview' => $ignoredEntity->reveal(),
      'view_mode_id' => 'teaser',
      'node' => $node->reveal(),
    ]);

    $this->routeMatch->getParameters()->willReturn($params);

    $key = $this->classInstance->generatePageKey();

    $this->assertEquals('node.1', $key);
  }

  /**
   * Use reflection to get the protected $cache.
   *
   * @return array
   *   The protected cache.
   */
  protected function getCache() {
    $reflectionObject = new \ReflectionObject($this->classInstance);
    $property = $reflectionObject->getProperty('cache');
    $property->setAccessible(TRUE);
    return $property->getValue($this->classInstance);
  }

  /**
   * Use reflection to set the protected $cache.
   *
   * @return array
   *   The protected cache.
   */
  protected function setCache($cache = []) {
    $reflectionObject = new \ReflectionObject($this->classInstance);
    $property = $reflectionObject->getProperty('cache');
    $property->setAccessible(TRUE);
    return $property->setValue($this->classInstance, $cache);
  }

}
